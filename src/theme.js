import { createMuiTheme, colors } from "@material-ui/core/";

export const theme = createMuiTheme({
  pallete: {
    primary: colors.cyan,
  },

  typography: {
    fontFamily: "Gilroy",
  },
});
