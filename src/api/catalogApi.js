import axios from "axios";
import { SERVER_URL_V1 } from "./apiConfig";

const SERVER_URL_OLD = "https://fakestoreapi.com/";

const getData = async (url) =>
  await axios.get(SERVER_URL_OLD + url).then((response) => response.data);

const getCategories = async (category) =>
  await getData(`products/categories${category ? category : ""}`);

const getProducts = async (id, setter) =>
  await axios.get(`${SERVER_URL_V1}products/${id ? id : ""}`).catch(() =>
    setter({
      type: "error",
      text: `no products with the id of ${id}`,
    })
  );

const createProduct = async (values, setter) => {
  await axios
    .post(`${SERVER_URL_V1}products`, values)
    .then((res) =>
      res.status === 201
        ? setter({ type: "success", text: "product has been added" })
        : null
    )
    .catch(() =>
      setter({
        type: "error",
        text: "something went wrong,please recheck the fields",
      })
    );
};

const updateProduct = async (id, values, setter) => {
  await axios
    .put(`${SERVER_URL_V1}products/${id}`, values)
    .then((res) =>
      res.status === 200
        ? setter({ type: "success", text: "product has been updated" })
        : null
    )
    .catch(() =>
      setter({
        type: "error",
        text: "something went wrong,please recheck the fields",
      })
    );
};

const removeProduct = async (id, setter) =>
  await axios
    .delete(`${SERVER_URL_V1}products/${id}`)
    .then((res) =>
      res.status === 204
        ? setter({ type: "success", text: "product has been removed" })
        : null
    )
    .catch(() =>
      setter({
        type: "error",
        text: `no products with the id of ${id}`,
      })
    );

export {
  getProducts,
  getCategories,
  createProduct,
  updateProduct,
  removeProduct,
};
