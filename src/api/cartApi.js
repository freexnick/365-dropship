import axios from "axios";
import { SERVER_URL_V1 } from "./apiConfig";

const SERVER_URL_V1_CART = SERVER_URL_V1 + "cart/";

axios.interceptors.request.use((req) => {
  req.headers.Authorization = `Bearer ${localStorage.getItem("userToken")}`;
  return req;
});

const addToCart = async (productId, qty) =>
  axios.post(`${SERVER_URL_V1_CART}add/`, { productId, qty });

const loadCart = async () => await axios.get(SERVER_URL_V1_CART);

const removeFromCart = async (id) =>
  axios.post(`${SERVER_URL_V1_CART}remove/${id}`);

const updateCart = async (id, qty) =>
  axios.post(`${SERVER_URL_V1_CART}update/${id}`, { qty });

export { addToCart, loadCart, removeFromCart, updateCart };
