import axios from "axios";
import { SERVER_URL } from "./apiConfig";

const authorize = async ({ email, password }, setter) =>
  await axios.post(`${SERVER_URL}login`, { email, password }).catch((err) => {
    if (err.response.status === 500) setter("user doesn't exist");
    if (err.response.status === 403) setter("incorrect password");
  });

const register = async ({ email, password }, setter) =>
  await axios
    .post(`${SERVER_URL}register`, {
      firstName: "empty",
      lastName: "empty",
      email,
      password,
      passwordConfirmation: password,
    })
    .catch((err) => {
      if (err.response.status === 400) {
        setter("account already exsist");
      }
    });

export { authorize, register };
