export const handleProductInputs = (e, properties, setter) => {
  switch (e.target.name) {
    case "title":
      return setter({ ...properties, title: e.target.value });
    case "description":
      return setter({ ...properties, description: e.target.value });
    case "price":
      return setter({ ...properties, price: e.target.value });
    case "imageUrl":
      return setter({ ...properties, imageUrl: e.target.value });
    default:
      return properties;
  }
};
