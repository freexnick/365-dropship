export const handleDrawer = (e, setter, state) => {
  if (e && setter) {
    e.stopPropagation();
    return setter(state);
  }
};
