import loadGif from "../assets/loading.gif";

export const handleBrokenImg = (e) => (e.target.src = loadGif);
