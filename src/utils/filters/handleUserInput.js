import { handleSortFilter } from "./handleSortFilter";
import { handleSearchFilter } from "./handleSearchFilter";
import { handleCategoryFilter } from "./handleCategoryFilter";
import { handleRangeFilter } from "./handleRangeFilter";

export const handleUserInput = (
  { sortType, searchTerm, selectedCategory, minPrice, maxPrice },
  items,
  setter
) => {
  let result = items;
  if (selectedCategory) result = handleCategoryFilter(result, selectedCategory);
  if (sortType) result = handleSortFilter(result, sortType);
  if ((minPrice, maxPrice))
    result = handleRangeFilter(result, minPrice, maxPrice);
  if (searchTerm) result = handleSearchFilter(result, searchTerm);
  setter(result);
};
