export const handleSortFilter = (items, sortType) => {
  switch (sortType) {
    case "desc":
      return items.sort((a, b) => b.title.localeCompare(a.title));
    case "asc":
      return items.sort((a, b) => a.title.localeCompare(b.title));
    case "low":
      return items.sort((a, b) => a.price - b.price);
    case "high":
      return items.sort((a, b) => b.price - a.price);
    default:
      return items;
  }
};
