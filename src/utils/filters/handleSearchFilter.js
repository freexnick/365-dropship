export const handleSearchFilter = (items, searchTerm) =>
  items.filter((product) =>
    product.title.trim().toLowerCase().includes(searchTerm.trim().toLowerCase())
  );
