export const handleRangeFilter = (items, min, max) => {
  if (max) return items.filter((item) => item.price > min && item.price <= max);
};
