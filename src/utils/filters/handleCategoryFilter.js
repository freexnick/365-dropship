export const handleCategoryFilter = (items, choosenCategory) =>
  items.filter(
    (product) =>
      product.category.toLowerCase() === choosenCategory.toLowerCase()
  );
