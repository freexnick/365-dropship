import { makeStyles } from "@material-ui/core/styles";
import loginBg from "../../../assets/login-bg.png";

export const useStyles = makeStyles((theme) => ({
  loginBackDrop: {
    width: "100%",
    height: "100%",
    backgroundImage: `url(${loginBg})`,
    opacity: ".92 ",
    backgroundSize: "cover",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
    "& > div:first-child": {
      backgroundColor: "inherit !important",
    },
  },
  authorizationForm: {
    width: "min(413px,95%)",
    backgroundColor: "#fff",
    textTransform: "capitalize",
    fontFamily: "Gilroy-Regular",
    boxShadow: "0 0 12px #6b6b6b",
    borderRadius: "8px",
    outline: "none",
    height: "520px",
    display: "flex",
    flexDirection: "column",
  },
  authorizationHeader: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  authorizationLogo: {
    margin: "30px",
    "& img": {
      width: "51px",
    },
  },
  authorizationTitle: {
    justifySelf: "center",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    color: "#171717",
  },
  loginForm: {
    color: "red",
    padding: "0 30px",
    "& input": {
      width: "100%",
      color: "#3a415d",
      height: "50px",
      borderRadius: "4px",
      border: "1px solid rgba(59,59,59,.2)",
      padding: "8px",
      margin: "10px 0",
      outline: "none",
      "&[type=submit]": {
        margin: "20px 0",
        backgroundColor: "#61d5df",
        color: "#fff",
        textDecoration: "capitalize",
      },
    },
  },
  authorizationFooter: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    gap: "5px",
    height: "100%",
  },
  authorizationFooterActive: {
    color: "#727272",
    fontWeight: "bold",
    cursor: "pointer",
  },
}));
