import { Modal } from "@material-ui/core/";
import { useAuthorizationContext } from "./AuthorizationContext";
import { FormHeader } from "./form/FormHeader";
import { FormBody } from "./form/FormBody";
import { FormFooter } from "./form/FormFooter";

const Authorization = () => {
  const { classes } = useAuthorizationContext();

  return (
    <Modal open className={classes.loginBackDrop}>
      <div className={classes.authorizationForm}>
        <FormHeader />
        <FormBody />
        <FormFooter />
      </div>
    </Modal>
  );
};

export default Authorization;
