import { createContext, useContext, useState } from "react";
import { authorize, register } from "../../../api/authorizationApi";
import { loadCart } from "../../../api/cartApi";
import { useHistory } from "react-router-dom";
import { useStyles } from "./authorizationStyles";
import { useEffect } from "react";

const AuthorizationContext = createContext();

const AuthorizationProvider = ({ children }) => {
  const userToken = localStorage.getItem("userToken");
  const [isLogged, setIsLogged] = useState(false);
  const [authFailed, setAuthFailed] = useState("");
  const history = useHistory();
  const classes = useStyles();

  const validateUser = async () => await loadCart(userToken, setIsLogged);

  const handleAuthorization = async (values, type) => {
    let result = {};
    if (type === "login") {
      result = await authorize(values, setAuthFailed);
    } else {
      result = await register(values, setAuthFailed);
    }
    if (result) {
      localStorage.setItem("user", JSON.stringify(result.data.data));
      localStorage.setItem("userToken", result.data.data.token);
      setIsLogged(true);
      setAuthFailed("");
      history.push("/");
    }
  };

  useEffect(() => {
    if (userToken) {
      setIsLogged(true);
    } else if (!userToken) {
      setIsLogged(false);
    }
  }, [userToken]);

  useEffect(() => {
    if (isLogged) history.push("/catalog");
  }, [isLogged]);

  return (
    <AuthorizationContext.Provider
      value={{
        isLogged,
        validateUser,
        authorize,
        setIsLogged,
        authFailed,
        setAuthFailed,
        handleAuthorization,
        classes,
        userToken,
      }}
    >
      {children}
    </AuthorizationContext.Provider>
  );
};

const useAuthorizationContext = () => useContext(AuthorizationContext);

export { AuthorizationContext, useAuthorizationContext, AuthorizationProvider };
