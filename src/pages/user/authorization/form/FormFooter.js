import { useAuthorizationContext } from "../AuthorizationContext";
import { useParams, useHistory } from "react-router-dom";

export const FormFooter = () => {
  const params = useParams();
  const history = useHistory();
  const { classes } = useAuthorizationContext();

  const handleAuthTypeChange = () => {
    history.push(`/user/${params.action === "login" ? "register" : "login"}`);
  };

  return (
    <div className={classes.authorizationFooter}>
      <span>
        {params.action === "login"
          ? "don't have account?"
          : "already have an account?"}
      </span>
      <span
        className={classes.authorizationFooterActive}
        onClick={handleAuthTypeChange}
      >
        {params.action === "login" ? "sign up" : "sign in"}
      </span>
    </div>
  );
};
