import { useAuthorizationContext } from "../AuthorizationContext";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { initialValues } from "./initialValues";
import { validationSchema } from "./validationSchema";
import { useParams } from "react-router-dom";

export const FormBody = () => {
  const params = useParams();
  const { handleAuthorization, authFailed, classes } =
    useAuthorizationContext();

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={(values) => handleAuthorization(values, params.action)}
    >
      <Form className={classes.loginForm}>
        <div className="user-login user-login--mail">
          <Field type="email" name="email" placeholder="enter your email" />
          <ErrorMessage name="email" />
        </div>
        <div className="user-login user-login--password">
          <Field
            type="password"
            name="password"
            placeholder="enter your password"
          />
          <ErrorMessage name="password" />
        </div>
        {authFailed ? <span>{authFailed}</span> : null}
        <div className="login-action">
          <input
            type="submit"
            value={params.action === "login" ? "Log In" : "Sign Up"}
            className="submit-button"
          />
        </div>
      </Form>
    </Formik>
  );
};
