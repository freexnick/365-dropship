import * as Yup from "yup";

export const validationSchema = Yup.object({
  email: Yup.string("enter your email")
    .email("Invalid email format")
    .required("Required"),
  password: Yup.string("enter your password").required("Required"),
});
