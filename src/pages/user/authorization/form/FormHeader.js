import logo from "../../../../assets/logo.png";
import { useAuthorizationContext } from "../AuthorizationContext";
import { useParams } from "react-router-dom";

export const FormHeader = () => {
  const params = useParams();
  const { classes } = useAuthorizationContext();

  return (
    <div className={classes.authorizationHeader}>
      <div className={classes.authorizationLogo}>
        <img src={logo} alt={logo} />
      </div>
      <div className={classes.authorizationTitle}>
        <h3>{params.action === "login" ? "members log in" : "sign up"}</h3>
      </div>
    </div>
  );
};
