import { useProductContext } from "./ProductContext";
import Product from "../layout/products/Product";

const ProductPreviewLayout = () => {
  const { productPreview } = useProductContext();

  return (
    <>
      {productPreview && (
        <Product
          {...productPreview}
          image={
            productPreview.imageUrl
              ? productPreview.imageUrl
              : productPreview.image
          }
        />
      )}
    </>
  );
};

export default ProductPreviewLayout;
