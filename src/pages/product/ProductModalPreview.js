import ModalLeftSide from "../layout/modal/modalLeftSide/ModalLeftSide";
import ModalRightSide from "../layout/modal/modalRightSide/ModalRightSide";
import { useLocation } from "react-router-dom";
import { useProductContext } from "./ProductContext";

const ProductModalPreview = () => {
  const location = useLocation();
  const { classes } = useProductContext();
  return (
    <div
      className={`modal-window ${
        location.pathname.includes("product") ? classes.productPageModal : null
      }`}
      onClick={(e) => e.stopPropagation()}
    >
      <ModalLeftSide />
      <ModalRightSide />
    </div>
  );
};

export default ProductModalPreview;
