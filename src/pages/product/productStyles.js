import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  productPageHeader: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: "100%",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
      marginBottom: "20px",
    },
  },
  productSearchForm: {
    width: "100%",
    height: "100%",
  },
  productPageSearch: {
    margin: "50px 0 10px",
    color: "red",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      marginLeft: "100px",
    },
  },
  productIdTitle: {
    "&  input[type='text']": {
      textAlign: "center ",
      [theme.breakpoints.down("md")]: {
        paddingBottom: "23px",
      },
    },
  },
  productHeaderOutput: {
    marginBottom: "20px",
    display: "flex",
    justifyContent: "center",
    fontFamily: "Gilroy-Medium",
  },
  productHeaderAlert: {
    width: "320px",
    [theme.breakpoints.down("md")]: {
      width: "285px",
    },
  },
  productHeaderAction: {
    position: "absolute",
    right: "0px",
    [theme.breakpoints.down("md")]: {
      width: "285px",
      position: "unset",
    },
  },
  productHeaderButton: {
    width: "90%",
    height: "50px",
    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
  },
  productRemoveButton: {
    width: "90%",
    height: "50px",
    marginTop: "4px",
    backgroundColor: "red",
    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
  },
  productPageMenu: {
    [theme.breakpoints.down("md")]: {
      marginRight: "100px",
    },
  },
  productPage: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  productLayout: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexFlow: "row wrap",
    justifyContent: "center",
  },
  creationForm: {
    display: "flex",
    flexDirection: "column",
    gap: "10px",
    width: "285px",
    color: "red",
    marginBottom: "8px",
  },
  productTextArea: {
    backgroundColor: "transparent",
    padding: "10px",
    resize: "none",
    borderColor: "#c8c8c8",
    borderRadius: "3px",
    "&:focus": {
      outlineColor: "#002884",
    },
  },
  productSubmit: {
    height: "32.5px",
    [theme.breakpoints.down("md")]: {
      height: "50px",
    },
  },
  productPageModal: {
    border: "2px solid #e4e5ec",
    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
    "&:hover": {
      border: "2px solid #61d5df",
    },
  },
}));
