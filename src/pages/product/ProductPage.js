import ProductPageHeader from "./form/ProductPageHeader";
import ProductPreviewLayout from "./ProductPreviewLayout";
import ProductCreationForm from "./form/ProductCreationForm";
import { useProductContext } from "./ProductContext";
import ProductModalPreview from "./ProductModalPreview";
import { useParams, useHistory } from "react-router-dom";
import { useEffect } from "react";

const ProductPage = () => {
  const params = useParams();
  const history = useHistory();
  const { classes, handleProductSearch, productPreview } = useProductContext();

  useEffect(() => {
    if (params.id) handleProductSearch(params.id);
  }, [params.id]);

  useEffect(() => {
    if (!productPreview) history.push("/product");
  }, [productPreview]);

  return (
    <div className={classes.productPage}>
      <ProductPageHeader />
      <div className={classes.productLayout}>
        <ProductCreationForm />
        <ProductPreviewLayout />
        <ProductModalPreview />
      </div>
    </div>
  );
};

export default ProductPage;
