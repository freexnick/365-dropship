import * as Yup from "yup";

export const formValidationSchema = Yup.object({
  title: Yup.string("enter product title").min(4).max(50).required("Required"),
  description: Yup.string("enter product description")
    .min(4)
    .max(950)
    .required("Required"),
  price: Yup.string("").min(1).required("Required"),
});
