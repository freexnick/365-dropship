import { Formik, Form, Field, ErrorMessage } from "formik";
import { useProductContext } from "../ProductContext";
import { TextField, Collapse, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import Alert from "@material-ui/lab/Alert";
import { useParams } from "react-router-dom";
import NavButton from "../../../common/Button";
import NavBarRightMenu from "../../catalog/main/contentNavBar/NavBarRight/NavBarRightMenu";

const ProductPageHeader = () => {
  const params = useParams();
  const {
    handleProductSearch,
    classes,
    productHeaderOutput,
    productPreview,
    handleProductSubmit,
    setProductHeaderOutput,
  } = useProductContext();

  return (
    <>
      <div className={classes.productPageHeader}>
        <div className={classes.productSearchForm}>
          <Formik
            initialValues={{ productId: params.id || "" }}
            onSubmit={({ productId }) => handleProductSearch(productId)}
          >
            <Form className={classes.productPageSearch}>
              <ErrorMessage name="productId" />
              <Field
                className={classes.productIdTitle}
                name="productId"
                as={TextField}
                placeholder="id"
              />

              <input
                type="submit"
                values="search for product"
                className={`product-search__button ${classes.productSubmit}`}
              />
              <div className={classes.productPageMenu}>
                <NavBarRightMenu />
              </div>
            </Form>
          </Formik>
          <div className="product-page__error">
            <Collapse
              className={classes.productHeaderOutput}
              in={Boolean(productHeaderOutput.type)}
            >
              <Alert
                severity={
                  productHeaderOutput.type ? productHeaderOutput.type : "info"
                }
                className={classes.productHeaderAlert}
                action={
                  <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                      setProductHeaderOutput({});
                    }}
                  >
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                }
              >
                {productHeaderOutput.text}
              </Alert>
            </Collapse>
          </div>
        </div>
        {params.id && (
          <div className={classes.productHeaderAction}>
            <span
              onClick={() =>
                handleProductSubmit(productPreview, params.id, "update")
              }
            >
              <NavButton
                className={`product-page__button ${classes.productHeaderButton}`}
                value="update item"
              />
            </span>
            <span
              onClick={() =>
                handleProductSubmit(productPreview, params.id, "remove")
              }
            >
              <NavButton
                className={`product-page__button ${classes.productRemoveButton}`}
                value="remove item"
              />
            </span>
          </div>
        )}
      </div>
    </>
  );
};

export default ProductPageHeader;
