import { Formik, Form, Field, ErrorMessage } from "formik";
import { useProductContext } from "../ProductContext";
import { TextField, TextareaAutosize } from "@material-ui/core";
import { handleProductInputs } from "../../../utils/handleProductInputs";
import { useParams } from "react-router-dom";

const ProductCreationForm = () => {
  const params = useParams();
  const {
    formValidationSchema,
    classes,
    productPreview,
    handleProductSubmit,
    setProductPreview,
  } = useProductContext();

  return (
    <Formik
      enableReinitialize
      initialValues={{
        title: productPreview.title || "",
        description: productPreview.description || "",
        price: productPreview.price || "",
        imageUrl: productPreview.imageUrl || "",
      }}
      validationSchema={formValidationSchema}
      onSubmit={(values, { resetForm }) => {
        params.id
          ? handleProductSubmit(values, params.id, "update")
          : handleProductSubmit(values);

        resetForm({});
      }}
    >
      {(formik) => {
        return (
          <Form className={classes.creationForm}>
            <Field
              name="title"
              as={TextField}
              label="Product Title"
              variant="outlined"
              InputLabelProps={{ shrink: true }}
              placeholder="Product Title"
              onChange={(e) => {
                handleProductInputs(e, productPreview, setProductPreview);
                formik.setFieldValue(e.target.name, e.target.value);
              }}
            />
            <ErrorMessage name="title" />
            <Field
              name="description"
              placeholder="Product Description"
              as={TextareaAutosize}
              rowsMin={28}
              rowsMax={28}
              label="description"
              className={classes.productTextArea}
              onChange={(e) => {
                handleProductInputs(e, productPreview, setProductPreview);
                formik.setFieldValue(e.target.name, e.target.value);
              }}
            />
            <ErrorMessage name="description" />
            <Field
              name="price"
              as={TextField}
              type="number"
              label="Price"
              placeholder="Product Price"
              variant="outlined"
              InputLabelProps={{ shrink: true }}
              onChange={(e) => {
                handleProductInputs(e, productPreview, setProductPreview);
                formik.setFieldValue(e.target.name, e.target.value);
              }}
            />
            <ErrorMessage name="price" />
            <Field
              name="imageUrl"
              as={TextField}
              variant="outlined"
              InputLabelProps={{ shrink: true }}
              placeholder="Product Image"
              label="Product Image"
              onChange={(e) => {
                handleProductInputs(e, productPreview, setProductPreview);
                formik.setFieldValue(e.target.name, e.target.value);
              }}
            />
            <ErrorMessage name="imageUrl" />
            <input
              type="submit"
              value={`${params.id ? "Edit Product" : "Add Product"}`}
              className={`product-footer__button ${classes.productSubmit}`}
              onSubmit={(values, { resetForm }) => {
                params.id
                  ? handleProductSubmit(values, params.id, "update")
                  : handleProductSubmit(values);

                resetForm({});
              }}
            />
          </Form>
        );
      }}
    </Formik>
  );
};

export default ProductCreationForm;
