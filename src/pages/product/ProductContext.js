import { createContext, useContext, useEffect, useState } from "react";
import {
  getProducts,
  createProduct,
  updateProduct,
  removeProduct,
} from "../../api/catalogApi";
import { formValidationSchema } from "./form/validationSchema";
import { useStyles } from "./productStyles";
import { useHistory } from "react-router-dom";
import { useCatalogContext } from "../catalog/CatalogContext";

const ProductContext = createContext();

const ProductContextProvider = ({ children }) => {
  const classes = useStyles();
  const history = useHistory();
  const [productPreview, setProductPreview] = useState([]);
  const [productHeaderOutput, setProductHeaderOutput] = useState({
    type: "",
    text: "",
  });
  const { setProductModal } = useCatalogContext();

  const handleProductSearch = async (id) => {
    setProductPreview([]);
    setProductHeaderOutput({});
    const result = await getProducts(id, setProductHeaderOutput);
    if (result) {
      setProductPreview(result.data.data);
      history.push(`/product/${id}`);
    } else {
      history.push("/product/");
    }
  };

  const handleProductSubmit = async (values, id, type) => {
    setProductHeaderOutput({});
    setProductPreview([]);
    if (type === "remove") {
      await removeProduct(id, setProductHeaderOutput);
    } else if (type === "update") {
      await updateProduct(id, values, setProductHeaderOutput);
    } else {
      await createProduct(values, setProductHeaderOutput);
    }
    history.push("/product");
  };

  useEffect(() => {
    setProductModal(productPreview);
  }, [productPreview]);

  return (
    <ProductContext.Provider
      value={{
        getProducts,
        formValidationSchema,
        productPreview,
        setProductPreview,
        handleProductSearch,
        classes,
        handleProductSubmit,
        productHeaderOutput,
        setProductHeaderOutput,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};

const useProductContext = () => useContext(ProductContext);

export { ProductContext, useProductContext, ProductContextProvider };
