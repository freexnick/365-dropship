import NavSortBarText from "./NavSortBarText";
import NavSortBarOptions from "./NavSortBarOptions";

const NavSortBar = () => (
  <div className="nav__sort">
    <NavSortBarText />
    <NavSortBarOptions />
  </div>
);

export default NavSortBar;
