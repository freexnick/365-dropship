import { useCatalogContext } from "../CatalogContext";

const NavSortBarOptions = () => {
  const { userInput, setUserInput } = useCatalogContext();
  return (
    <>
      <select
        id="sort"
        className="nav__sort-list"
        value={userInput.sortType}
        onChange={(e) =>
          setUserInput({ ...userInput, sortType: e.target.value })
        }
      >
        <option value="asc">Ascending</option>
        <option value="desc">Descending</option>
        <option value="low">Price: Low to High</option>
        <option value="high">Price: High to Low</option>
      </select>
    </>
  );
};

export default NavSortBarOptions;
