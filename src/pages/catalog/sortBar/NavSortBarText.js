const NavSortBarText = () => (
  <>
    <span className="nav__sort-text nav__sort-text--dropship"></span>
    <span className="nav__sort-text">Sort By:</span>
  </>
);

export default NavSortBarText;
