import { useEffect } from "react";
import { useCatalogContext } from "./CatalogContext";
import { useParams } from "react-router-dom";
import Main from "./main/Main";
import Modal from "../layout/modal/Modal";
import { handleUserInput } from "../../utils/filters/handleUserInput";
import SidebarDrawer from "../../common/SidebarDrawer";

const ProductCatalog = () => {
  const params = useParams();
  const {
    products,
    userInput,
    setDisplayProducts,
    setSelectedProduct,
    setSidebarDrawer,
    matchesSidebarDrawer,
  } = useCatalogContext();

  useEffect(() => {
    if (products) handleUserInput(userInput, products, setDisplayProducts);
  }, [userInput]);

  useEffect(() => {
    setSelectedProduct([]);
  }, [userInput]);

  useEffect(() => {
    if (matchesSidebarDrawer) {
      setSidebarDrawer(false);
    } else {
      setSidebarDrawer(true);
    }
  }, [matchesSidebarDrawer]);

  return (
    <div className="product-catalog">
      {params.id && <Modal />}
      <SidebarDrawer />
      <Main />
    </div>
  );
};

export default ProductCatalog;
