import NavButton from "../../../common/Button";
import { useCatalogContext } from "../CatalogContext";

const ResetFilter = ({ setCurrentPriceValue, setCurrentProfitValue }) => {
  const { setUserInput, userInput } = useCatalogContext();
  const clearCurrentRangeValues = () => {
    setCurrentPriceValue([0, 1000]);
    setCurrentProfitValue([0, 100]);
    setUserInput({
      ...userInput,
      minPrice: 0,
      maxPrice: 1000,
      minProfit: 0,
      maxProfit: 100,
    });
  };
  return (
    <div className="sidebar-reset" onClick={clearCurrentRangeValues}>
      <NavButton className="sidebar-reset__button" value="reset filter" />
    </div>
  );
};

export default ResetFilter;
