const DropdownCategory = ({ handleDropdown, toggleDropdown }) => {
  return (
    <div className="sidebar__dropdown-item sidebar__dropdown-item--category">
      <div
        className="sidebar__dropdown-title sidebar__dropdown-title--category"
        onClick={() => handleDropdown("category")}
      >
        <span className="sidebar__dropdown-text">Choose Category</span>
        <span
          className={`sidebar__dropdown-arrow ${
            toggleDropdown.includes("category")
              ? "sidebar__dropdown-arrow--toggled"
              : ""
          }`}
        ></span>
      </div>
      <span
        className={`sidebar__dropdown-categories  sidebar__dropdown-categories--${
          toggleDropdown.includes("category") ? "active" : "hidden"
        }`}
        onClick={() => handleDropdown("category")}
      >
        ...
      </span>
    </div>
  );
};

export default DropdownCategory;
