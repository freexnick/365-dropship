import { getCategories } from "../../../../api/catalogApi";
import { Link, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useCatalogContext } from "../../CatalogContext";

const DropdownNiche = ({ handleDropdown, toggleDropdown }) => {
  const params = useParams();
  const { userInput, setUserInput } = useCatalogContext();
  const categories = JSON.parse(localStorage.getItem("categories"));
  const [categoryList, setCategoryList] = useState();

  const loadCategories = async () => {
    if (!categories) {
      const categoryList = await getCategories();
      localStorage.setItem("categories", JSON.stringify(categoryList));
      setCategoryList(categoryList);
    } else {
      setCategoryList(categories);
    }
  };
  const handleCategorySelection = (selection) => {
    handleDropdown("niche");
    setUserInput({ ...userInput, selectedCategory: selection });
  };

  useEffect(() => {
    loadCategories();
  }, []);

  useEffect(() => {
    if (params.category && params.category !== "all") {
      setUserInput({
        ...userInput,
        selectedCategory: params.category.replace(/-/g, " "),
      });
    }
  }, [params.category]);
  return (
    <div className="sidebar__dropdown-item sidebar__dropdown-item--niche">
      <div
        className="sidebar__dropdown-title sidebar__dropdown-title--niche"
        onClick={() => handleDropdown("niche")}
      >
        <span className="sidebar__dropdown-text">
          {userInput.selectedCategory
            ? userInput.selectedCategory
            : "Choose Niche"}
        </span>
        <span
          className={`sidebar__dropdown-arrow ${
            toggleDropdown.includes("niche")
              ? "sidebar__dropdown-arrow--toggled"
              : ""
          }`}
        ></span>
      </div>
      <ul className="niche-list">
        <Link to="/">
          <li
            className={`niche-list__item  niche-list__item--${
              toggleDropdown.includes("niche") ? "active" : "hidden"
            }`}
            onClick={() => handleCategorySelection("")}
          >
            ...
          </li>
        </Link>
        {categoryList &&
          categoryList.map((categoryName) => (
            <Link
              to={`/catalog/${categoryName.replace(/ /g, "-")}`}
              key={categoryName}
            >
              <li
                className={`niche-list__item  niche-list__item--${
                  toggleDropdown.includes("niche") ? "active" : "hidden"
                }`}
                onClick={() => handleCategorySelection(categoryName)}
              >
                {categoryName}
              </li>
            </Link>
          ))}
      </ul>
    </div>
  );
};

export default DropdownNiche;
