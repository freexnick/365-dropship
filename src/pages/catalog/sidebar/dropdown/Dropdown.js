import { useState } from "react";
import DropdownNiche from "./DropdownNiche";
import DropdownCategory from "./DropdownCategory";
import { useCatalogContext } from "../../CatalogContext";
const Dropdown = () => {
  const [toggleDropdown, setToggleDropdown] = useState([]);
  const { products } = useCatalogContext();

  const handleDropdown = (target) => {
    setToggleDropdown([target, ...toggleDropdown]);
    if (toggleDropdown.includes(target)) {
      setToggleDropdown(toggleDropdown.filter((item) => item !== target));
    }
  };
  return (
    <div className="sidebar__dropdown">
      {products && (
        <DropdownNiche
          handleDropdown={handleDropdown}
          toggleDropdown={toggleDropdown}
        />
      )}
      <DropdownCategory
        handleDropdown={handleDropdown}
        toggleDropdown={toggleDropdown}
      />
    </div>
  );
};

export default Dropdown;
