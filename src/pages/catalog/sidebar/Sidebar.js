import { useState } from "react";
import DropDown from "./dropdown/Dropdown";
import SelectList from "./selectlist/SelectList";
import Range from "./range/Range";
import ResetFilter from "./ResetFilter";
import NavButton from "../../../common/Button";
import { useCatalogContext } from "../CatalogContext";
import { handleDrawer } from "../../../utils/handleDrawer";

const Sidebar = () => {
  const [currentPriceValue, setCurrentPriceValue] = useState([0, 1000]);
  const [currentProfitValue, setCurrentProfitValue] = useState([0, 100]);
  const { matchesSidebarDrawer, setSidebarDrawer } = useCatalogContext();
  return (
    <div className="sidebar">
      <DropDown />
      {matchesSidebarDrawer ? (
        <div
          className="sidebar-back"
          onClick={(e) => handleDrawer(e, setSidebarDrawer, false)}
        >
          <NavButton className="sidebar-back__button" value="back" />
        </div>
      ) : null}
      <SelectList />
      <Range
        currentPriceValue={currentPriceValue}
        currentProfitValue={currentProfitValue}
        setCurrentPriceValue={setCurrentPriceValue}
        setCurrentProfitValue={setCurrentProfitValue}
      />
      <ResetFilter
        setCurrentPriceValue={setCurrentPriceValue}
        setCurrentProfitValue={setCurrentProfitValue}
      />
    </div>
  );
};

export default Sidebar;
