import SingleRange from "./SingleRange";
import { useCatalogContext } from "../../CatalogContext";

const Range = ({
  currentPriceValue,
  currentProfitValue,
  setCurrentPriceValue,
  setCurrentProfitValue,
}) => {
  const { userInput, setUserInput } = useCatalogContext();
  const handlePriceFilter = (e, value) => {
    setCurrentPriceValue(value);
    setUserInput({ ...userInput, minPrice: value[0], maxPrice: value[1] });
  };

  const handleProfitFilter = (e, value) => {
    setCurrentProfitValue(value);
    setUserInput({ ...userInput, minProfit: value[0], maxProfit: value[1] });
  };
  return (
    <div className="sidebar__slider">
      <SingleRange
        type={"$"}
        modifier={"price"}
        min={0}
        max={1000}
        handlePriceFilter={handlePriceFilter}
        value={currentPriceValue}
        currentValue={currentPriceValue}
      />
      <SingleRange
        type={"%"}
        modifier={"profit"}
        min={0}
        max={100}
        currentValue={currentProfitValue}
        handleProfitFilter={handleProfitFilter}
        value={currentProfitValue}
      />
    </div>
  );
};

export default Range;
