import Slider from "@material-ui/core/Slider";

const SingleRange = ({
  min,
  max,
  type,
  modifier,
  value,
  handlePriceFilter,
  handleProfitFilter,
  currentValue,
}) => {
  return (
    <div className={`sidebar__slider-item sidebar__slider-item--${modifier}`}>
      <span className="sidebar__slider-title">{modifier} range</span>
      <Slider
        min={min}
        max={max}
        value={value}
        onChange={modifier === "price" ? handlePriceFilter : handleProfitFilter}
      />
      <div className="sidebar__value">
        <div className="sidebar__value-left">
          <span className="sidebar__value-item sidebar__value-item--type">
            {type}
          </span>
          <span className="sidebar__value-item sidebar__value-item--min">
            {currentValue[0]}
          </span>
        </div>
        <div className="sidebar__value-right">
          <span className="sidebar__value-item sidebar__value-item--type">
            {type}
          </span>
          <span className="sidebar__value-item sidebar__value-item--min">
            {currentValue[1]}
          </span>
        </div>
      </div>
    </div>
  );
};

export default SingleRange;
