import SingleSelecItem from "./SingleSelectItem";

const SelectList = () => {
  return (
    <div className="sidebar__select">
      <SingleSelecItem placeholder={"Ship From"} modifier={"from"} />
      <SingleSelecItem placeholder={"Ship To"} modifier={"to"} />
      <SingleSelecItem placeholder={"Select Supplier"} modifier={"supplier"} />
    </div>
  );
};

export default SelectList;
