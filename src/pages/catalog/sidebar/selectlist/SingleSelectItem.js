const SingleSelectItem = ({ modifier, placeholder }) => {
  return (
    <div className={`sidebar__select-item sidebar__select-item--${modifier}`}>
      <select>
        <option value={placeholder} default>
          {placeholder}
        </option>
      </select>
    </div>
  );
};

export default SingleSelectItem;
