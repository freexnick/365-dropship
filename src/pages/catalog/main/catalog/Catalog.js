import ProductList from "../../../../common/ProductList";
import Loading from "../../../../common/Loading";
import { useCatalogContext } from "../../CatalogContext";

const Catalog = () => {
  const { isLoading } = useCatalogContext();
  return (
    <section className="catalog">
      {isLoading && <Loading quantity={20} />}
      <ProductList />
    </section>
  );
};
export default Catalog;
