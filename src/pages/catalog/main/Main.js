import NavBar from "./contentNavBar/NavBar";
import SortBar from "../sortBar/SortBar";
import ProductCatalog from "./catalog/Catalog";

const Main = () => {
  return (
    <div className="main">
      <NavBar />
      <SortBar />
      <ProductCatalog />
    </div>
  );
};

export default Main;
