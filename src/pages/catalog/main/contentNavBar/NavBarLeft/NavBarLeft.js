import NavBarSelectedButton from "./NavBarSelectedButton";
import NavBarLeftText from "./NavBarLeftText";
import NavBarClearButton from "./NavBarClearButton";
import NavBarFilterButton from "./NavBarFilterButton";
import { useCatalogContext } from "../../../CatalogContext";

const NavBarLeft = () => {
  const { selectedProduct, matchesSidebarDrawer } = useCatalogContext();
  return (
    <div className="nav__header-left">
      <NavBarSelectedButton />
      {matchesSidebarDrawer ? <NavBarFilterButton /> : null}
      <NavBarLeftText />
      {selectedProduct.length ? <NavBarClearButton /> : null}
    </div>
  );
};

export default NavBarLeft;
