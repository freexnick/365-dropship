import NavButton from "../../../../../common/Button";
import { useCatalogContext } from "../../../CatalogContext";

const NavBarClearButton = () => {
  const { handleAllProductClear } = useCatalogContext();
  return (
    <div className="nav__header-clearAll" onClick={handleAllProductClear}>
      <NavButton
        className="nav__header-button nav__header-button--clear"
        value="Clear Selected"
      />
    </div>
  );
};

export default NavBarClearButton;
