import NavButton from "../../../../../common/Button";
import { useCatalogContext } from "../../../CatalogContext";

const NavBarSelectedButton = () => {
  const { handleAllProductSelect } = useCatalogContext();
  return (
    <>
      <div className="nav__header-selectAll" onClick={handleAllProductSelect}>
        <NavButton
          className="nav__header-button nav__header-button--select"
          value="Select All"
        />
      </div>
    </>
  );
};

export default NavBarSelectedButton;
