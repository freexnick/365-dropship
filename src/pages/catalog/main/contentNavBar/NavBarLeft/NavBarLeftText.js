import { useState } from "react";
import selecAllIcon from "../../../../../assets/selectallico.png";
import { useCatalogContext } from "../../../CatalogContext";
import { useLocation } from "react-router-dom";

export const NavBarLeftText = () => {
  const location = useLocation();
  const {
    handleAllProductSelect,
    setSelectedProduct,
    selectedProduct,
    displayProducts,
    cartItems,
  } = useCatalogContext();
  const [selectProductsIco, setSelectproductsIco] = useState(false);
  const handleSelectProductsIco = () => {
    handleAllProductSelect();
    setSelectproductsIco(!selectProductsIco);
    if (selectProductsIco) {
      setSelectedProduct([]);
    }
  };
  return (
    <>
      <span className="nav__header-text nav__header-text--separator"></span>
      <span className="nav__header-text" id="productAmount">
        <span className="nav__header-text nav__header-text--hide">
          selected
        </span>
        <span className="nav__header-text nav__header-text--number">
          {selectedProduct.length ? selectedProduct.length : 0}
        </span>
        <span className="nav__header-text nav__header-text--hide">out of</span>
        {location.pathname.includes("catalog") ? (
          <span className="nav__header-text nav__header-text--amount">
            {displayProducts.length > 1
              ? `${displayProducts.length} products`
              : `${displayProducts.length} product`}
          </span>
        ) : (
          <span className="nav__header-text nav__header-text--amount">
            {cartItems.length > 1
              ? `${cartItems.length} products`
              : `${cartItems.length} product`}
          </span>
        )}
      </span>
      <img
        className="nav__header-selectIco"
        src={selecAllIcon}
        alt="select All"
        onClick={handleSelectProductsIco}
      />
    </>
  );
};

export default NavBarLeftText;
