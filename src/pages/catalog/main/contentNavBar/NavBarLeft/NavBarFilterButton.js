import NavButton from "../../../../../common/Button";
import filter from "../../../../../assets/filter.png";
import { useCatalogContext } from "../../../CatalogContext";
import { handleDrawer } from "../../../../../utils/handleDrawer";
import { useMediaQuery, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  filterIcon: {
    [theme.breakpoints.only("xs")]: {
      height: "100%",
    },
  },
}));

const NavBarFilterButton = () => {
  const classes = useStyles();
  const { setSidebarDrawer } = useCatalogContext();
  const matches = useMediaQuery((theme) => theme.breakpoints.only("sm"));
  return (
    <div
      className={`nav__header-filter ${classes.filterIcon}`}
      onClick={(e) => handleDrawer(e, setSidebarDrawer, true)}
    >
      {matches ? (
        <NavButton
          className="nav__header-button nav__header-button--filter"
          value="filter"
        />
      ) : (
        <img className="nav__header-filterIco" alt="filter" src={filter} />
      )}
    </div>
  );
};

export default NavBarFilterButton;
