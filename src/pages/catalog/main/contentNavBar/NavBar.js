import { useState } from "react";
import NavBarLeft from "./NavBarLeft/NavBarLeft";
import NavBarRight from "./NavBarRight/NavBarRight";
import { useCatalogContext } from "../../CatalogContext";
import { useLocation } from "react-router-dom";

const NavBar = () => {
  const location = useLocation();
  const { products, cartItems } = useCatalogContext();
  const [toggledSearch, setToggledSerach] = useState(false);
  const toggleSearch = () => {
    setToggledSerach(!toggledSearch);
  };
  return (
    <div className="nav__header">
      {(location.pathname.includes("catalog") ? products : cartItems) && (
        <NavBarLeft />
      )}
      <NavBarRight toggledSearch={toggledSearch} toggleSearch={toggleSearch} />
    </div>
  );
};
export default NavBar;
