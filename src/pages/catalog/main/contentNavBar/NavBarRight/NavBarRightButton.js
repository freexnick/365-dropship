import NavButton from "../../../../../common/Button";
import { useCatalogContext } from "../../../CatalogContext";
import { useLocation } from "react-router-dom";

const NavBarRightButton = ({ toggledSearch }) => {
  const { addAllItemsToCart, removeAllItemsFromCart } = useCatalogContext();
  const location = useLocation();
  return (
    !toggledSearch && (
      <div
        className="nav-bar__add"
        onClick={
          location.pathname.includes("catalog")
            ? addAllItemsToCart
            : removeAllItemsFromCart
        }
      >
        <NavButton
          className="nav__header-button nav__header-button--right"
          value={
            location.pathname.includes("catalog")
              ? "add to inventory"
              : "remove from inventory"
          }
        />
      </div>
    )
  );
};

export default NavBarRightButton;
