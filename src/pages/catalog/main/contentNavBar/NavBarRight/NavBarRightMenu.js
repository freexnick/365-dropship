import IconButton from "@material-ui/core/IconButton";
import { Menu, HelpOutlineOutlined } from "@material-ui/icons/";
import { useNavContext } from "../../../../layout/nav/NavContext";
import { useMediaQuery, makeStyles } from "@material-ui/core/";
import { handleDrawer } from "../../../../../utils/handleDrawer";

const useStyles = makeStyles({
  productNavItem: {
    "& svg": {
      fill: "#545e85",
    },
  },
});

const NavBarRightMenu = () => {
  const { setNavDrawer } = useNavContext();
  const classes = useStyles();
  const matches = useMediaQuery((theme) => theme.breakpoints.down("md"));
  return (
    <>
      {matches ? (
        <IconButton
          onClick={(e) => handleDrawer(e, setNavDrawer, true)}
          className={classes.productNavItem}
        >
          <Menu />
        </IconButton>
      ) : null}
      {!matches ? (
        <IconButton className={classes.productNavItem}>
          <HelpOutlineOutlined />
        </IconButton>
      ) : null}
    </>
  );
};

export default NavBarRightMenu;
