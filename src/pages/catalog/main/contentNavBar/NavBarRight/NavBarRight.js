import NavBarSearch from "./NavBarSearch";
import NavBarRightButton from "./NavBarRightButton";
import NavBarRightMenu from "./NavBarRightMenu";

const NavBarRight = (props) => (
  <div className="nav__header-right">
    <NavBarSearch {...props} />
    <NavBarRightButton {...props} />
    <NavBarRightMenu />
  </div>
);

export default NavBarRight;
