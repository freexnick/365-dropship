import { createContext, useContext, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { useMediaQuery } from "@material-ui/core";
import { addToCart, removeFromCart } from "../../api/cartApi";
import { useInventoryContext } from "../inventory/InventoryContext";

const CatalogContext = createContext();

const CatalogProvider = ({ children }) => {
  const products = JSON.parse(localStorage.getItem("products"));
  const location = useLocation();
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(true);
  const [productModal, setProductModal] = useState("");
  const [displayProducts, setDisplayProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState([]);
  const [sidebarDrawer, setSidebarDrawer] = useState(true);
  const [userInput, setUserInput] = useState({
    sortType: "asc",
    searchTerm: "",
    selectedCategory: "",
    minPrice: 0,
    maxPrice: 0,
    minProfit: 0,
    maxProfit: 0,
  });
  const { cartItems, setCartItems } = useInventoryContext();

  const matchesSidebarDrawer = useMediaQuery((theme) =>
    theme.breakpoints.down("sm")
  );

  const handleAllProductSelect = () =>
    setSelectedProduct(
      (location.pathname.includes("catalog") ? displayProducts : cartItems).map(
        (product) => product.id
      )
    );

  const handleAllProductClear = () => setSelectedProduct([]);

  const handleProductModal = (e, product) => {
    e.stopPropagation();
    setProductModal(product);
    history.replace(
      `${location.pathname.includes("catalog") ? "/catalog" : "/inventory"}/${
        userInput.selectedCategory
          ? userInput.selectedCategory.replace(/ /g, "-")
          : "all"
      }/${product.id}`
    );
  };

  const handleModalBackdrop = () => {
    history.push(
      `/${
        location.pathname.includes("catalog") ? "catalog" : "inventory"
      }/${userInput.selectedCategory.replace(/ /g, "-")}`
    );
    setProductModal("");
  };

  const addAllItemsToCart = async () => {
    let result = [];
    for (const id of selectedProduct) {
      result = await addToCart(id, 1);
    }
    setSelectedProduct("");
    if (result) setCartItems(result.data.data.cartItem.items);
  };

  const removeAllItemsFromCart = async () => {
    let result = [];
    for (const id of selectedProduct) {
      result = await removeFromCart(id);
    }
    setSelectedProduct("");
    if (result) setCartItems(result.data.data.cartItem.items);
  };

  return (
    <CatalogContext.Provider
      value={{
        products,
        isLoading,
        setIsLoading,
        productModal,
        setProductModal,
        displayProducts,
        setDisplayProducts,
        selectedProduct,
        setSelectedProduct,
        userInput,
        setUserInput,
        handleAllProductSelect,
        handleAllProductClear,
        handleProductModal,
        handleModalBackdrop,
        history,
        sidebarDrawer,
        setSidebarDrawer,
        matchesSidebarDrawer,
        addAllItemsToCart,
        removeAllItemsFromCart,
        cartItems,
        setCartItems,
      }}
    >
      {children}
    </CatalogContext.Provider>
  );
};

const useCatalogContext = () => useContext(CatalogContext);

export { CatalogProvider, useCatalogContext, CatalogContext };
