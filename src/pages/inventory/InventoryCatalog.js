import Product from "../layout/products/Product";
import { useInventoryContext } from "./InventoryContext";
import Loading from "../../common/Loading";
import NavBar from "../catalog/main/contentNavBar/NavBar";
import SortBar from "../catalog/sortBar/SortBar";

const InventoryCatalog = () => {
  const { cartItems, isLoading } = useInventoryContext();
  return (
    <div className="main">
      <NavBar />
      <SortBar />
      <section className="catalog">
        {isLoading && <Loading quantity={20} />}
        {cartItems
          ? cartItems.map((product) => (
              <Product key={product.id} {...product} product={product} />
            ))
          : null}
      </section>
    </div>
  );
};

export default InventoryCatalog;
