import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  inventoryCatalog: {
    display: "flex",
    height: "100%",
    width: "100%",
  },
}));
