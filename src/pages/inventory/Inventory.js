import SidebarDrawer from "../../common/SidebarDrawer";
import InventoryCatalog from "./InventoryCatalog";
import { useStyles } from "./inventoryStyles";
import Modal from "../layout/modal/Modal";
import { useParams } from "react-router-dom";
import { useCatalogContext } from "../catalog/CatalogContext";
import { useEffect } from "react";

const Inventory = () => {
  const classes = useStyles();
  const params = useParams();
  const { setSidebarDrawer, matchesSidebarDrawer } = useCatalogContext();

  useEffect(() => {
    if (matchesSidebarDrawer) {
      setSidebarDrawer(false);
    } else {
      setSidebarDrawer(true);
    }
  }, [matchesSidebarDrawer]);

  return (
    <>
      {params.id && <Modal />}
      <div className={classes.inventoryCatalog}>
        <SidebarDrawer />
        <InventoryCatalog />
      </div>
    </>
  );
};

export default Inventory;
