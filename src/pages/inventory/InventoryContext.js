import { createContext, useContext } from "react";
import {
  addToCart,
  removeFromCart,
  loadCart,
  updateCart,
} from "../../api/cartApi";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";

const InventoryContext = createContext();

const InventoryProvider = ({ children }) => {
  const location = useLocation();
  const [cartItems, setCartItems] = useState([]);

  const displayCartItems = async () => {
    const result = await loadCart();
    if (result) setCartItems(result.data.data.cartItem.items);
  };

  const handleCartAction = async ({ id, qty = 1 }) => {
    let result = [];
    let pathname = location.pathname.includes("catalog");
    if (pathname) {
      result = await addToCart(id, qty);
    } else if (qty - 1 <= 0 && !pathname) {
      result = await removeFromCart(id);
    } else if (!pathname) {
      result = await updateCart(id, qty - 1);
    }
    if (result) setCartItems(result.data.data.cartItem.items);
  };

  useEffect(() => displayCartItems(), []);

  return (
    <InventoryContext.Provider
      value={{ handleCartAction, cartItems, displayCartItems, setCartItems }}
    >
      {children}
    </InventoryContext.Provider>
  );
};

const useInventoryContext = () => useContext(InventoryContext);

export { InventoryContext, InventoryProvider, useInventoryContext };
