import { createContext, useContext, useState, useEffect } from "react";
import { useMediaQuery } from "@material-ui/core/";

const NavContext = createContext();

const NavProvider = ({ children }) => {
  const matchesNavDrawer = useMediaQuery((theme) =>
    theme.breakpoints.down("md")
  );
  const [navDrawer, setNavDrawer] = useState(true);

  useEffect(() => {
    if (matchesNavDrawer) {
      setNavDrawer(false);
    } else {
      setNavDrawer(true);
    }
  }, [matchesNavDrawer]);

  return (
    <NavContext.Provider value={{ navDrawer, setNavDrawer, matchesNavDrawer }}>
      {children}
    </NavContext.Provider>
  );
};

const useNavContext = () => useContext(NavContext);

export { NavContext, NavProvider, useNavContext };
