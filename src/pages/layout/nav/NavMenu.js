import logo from "../../../assets/logo.png";
import Menu from "./menu/Menu";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";
import { useNavContext } from "./NavContext";
import { handleDrawer } from "../../../utils/handleDrawer";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  logo: {
    width: "100%",
    [theme.breakpoints.down("md")]: {
      width: "35%",
    },
  },
}));

const NavMenu = () => {
  const classes = useStyles();
  const { matchesNavDrawer, setNavDrawer } = useNavContext();
  return (
    <div className="header" onClick={(e) => e.stopPropagation()}>
      <div className="header__logo">
        <img className={classes.logo} src={logo} alt="logo" />
        {matchesNavDrawer ? (
          <IconButton onClick={(e) => handleDrawer(e, setNavDrawer, false)}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </div>
      <Menu />
    </div>
  );
};

export default NavMenu;
