import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  makeStyles,
} from "@material-ui/core";
import { useLocation, Link } from "react-router-dom";
import { menuItems } from "./menuItems";

const useStyles = makeStyles((theme) => ({
  listItem: {
    margin: "15px 0",
    "&:hover": {
      backgroundColor: "unset",
    },
  },
  activeIcon: {
    "& svg": {
      fill: "#49547d",
      "&:hover": {
        fill: "#61d5df",
      },
    },
  },
  selected: {
    borderLeft: "2px solid #61d5df",
    backgroundColor: "#f7fdfe",
    "& svg": {
      fill: "#61d5df",
    },
  },
  listItemText: {
    textTransform: "capitalize",
    display: "none",
    [theme.breakpoints.down("md")]: {
      display: "inline-block",
    },
  },
}));

const Menu = () => {
  const classes = useStyles();

  const location = useLocation();
  return (
    <>
      <nav>
        <List>
          {menuItems.map((item) => (
            <Link to={item.path} key={item.text}>
              <ListItem
                className={`${classes.listItem} ${
                  location.pathname.includes(item.path) ? classes.selected : ""
                }`}
                button
              >
                <ListItemText
                  className={classes.listItemText}
                  primary={item.text}
                />
                <ListItemIcon className={classes.activeIcon}>
                  {item.icon}
                </ListItemIcon>
              </ListItem>
            </Link>
          ))}
        </List>
      </nav>
    </>
  );
};

export default Menu;
