import {
  AccountCircle,
  Speed,
  List,
  Layers,
  ShoppingCart,
  AssignmentTurnedIn,
  SwapHorizontalCircle,
  Assignment,
  PostAdd,
} from "@material-ui/icons";

export const menuItems = [
  { text: "profile", icon: <AccountCircle />, path: "/profile" },
  { text: "dashboard", icon: <Speed />, path: "/dashboard" },
  { text: "catalog", icon: <List />, path: "/catalog" },
  { text: "inventory", icon: <Layers />, path: "/inventory" },
  { text: "cart", icon: <ShoppingCart />, path: "/cart" },
  { text: "orders", icon: <AssignmentTurnedIn />, path: "/orders" },
  {
    text: "transactions",
    icon: <SwapHorizontalCircle />,
    path: "/transactions ",
  },
  { text: "stores List", icon: <Assignment />, path: "/stores-list" },
  { text: "product page", icon: <PostAdd />, path: "/product" },
];
