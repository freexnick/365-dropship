import { getProducts } from "../../../api/catalogApi";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import ModalLeftSide from "./modalLeftSide/ModalLeftSide";
import ModalRightSide from "./modalRightSide/ModalRightSide";
import { useCatalogContext } from "../../catalog/CatalogContext";

const Modal = () => {
  const params = useParams();
  const { productModal, setProductModal, handleModalBackdrop } =
    useCatalogContext();

  const singleProduct = async () => {
    if (params.id) {
      const item = await getProducts(params.id);
      setProductModal(item.data.data);
    }
  };

  useEffect(() => {
    if (!productModal ? singleProduct() : "");
  }, [params.id]);

  return (
    <>
      {productModal && (
        <div
          className="modal-wrapper"
          key={productModal.id}
          onClick={handleModalBackdrop}
        >
          <div className="modal-window" onClick={(e) => e.stopPropagation()}>
            <ModalLeftSide />
            <ModalRightSide />
          </div>
        </div>
      )}
    </>
  );
};

export default Modal;
