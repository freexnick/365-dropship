const ModalHeader = () => {
  return (
    <div className="modal__header">
      <div className="modal__header-left">
        <span className="modal__header-item modal__header-item--id">
          SKU# bgb-i2500321
        </span>
        <span className="modal__header-item modal__header-item--copy">
          Copy
        </span>
      </div>
      <div className="modal__header-right">
        <span className="modal__header-item modal__header-item--by">
          Supplier:
        </span>
        <span className="modal__header-item modal__header-item--supplier">
          SP-Supplier115
        </span>
      </div>
    </div>
  );
};

export default ModalHeader;
