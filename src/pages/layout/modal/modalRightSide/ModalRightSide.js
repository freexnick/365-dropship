import ModalCloseButton from "./ModalCloseButton";
import ModalHeader from "./ModalHeader";
import ModalTitle from "./ModalTitle";
import ModalAddButton from "./ModalAddButton";
import ModalMenu from "./ModalMenu";
import ModalDescription from "./ModalDescription";
import { useLocation } from "react-router-dom";

const ModalRightSide = () => {
  const location = useLocation();
  return (
    <div className="modal__overview">
      {!location.pathname.includes("product") ? <ModalCloseButton /> : null}
      <div className="modal__info">
        <ModalHeader />
        <ModalTitle />
        <ModalAddButton />
        <ModalMenu />
        <ModalDescription />
      </div>
    </div>
  );
};

export default ModalRightSide;
