import CloseIcon from "@material-ui/icons/Close";
import { useCatalogContext } from "../../../catalog/CatalogContext";
import { useLocation } from "react-router-dom";

const ModalCloseButton = () => {
  const location = useLocation();
  const { handleModalBackdrop } = useCatalogContext();
  return (
    <div
      className="modal__close"
      onClick={
        location.pathname.includes("catalog") ||
        location.pathname.includes("inventory")
          ? handleModalBackdrop
          : null
      }
    >
      <CloseIcon
        className="modal__close-img"
        onClick={
          location.pathname.includes("catalog") ||
          location.pathname.includes("inventory")
            ? handleModalBackdrop
            : null
        }
      />
    </div>
  );
};

export default ModalCloseButton;
