const ModalMenu = () => {
  return (
    <div className="modal__description-details">
      <ul className="modal__description-items">
        <li className="modal__description-item">Product Details</li>
      </ul>
    </div>
  );
};

export default ModalMenu;
