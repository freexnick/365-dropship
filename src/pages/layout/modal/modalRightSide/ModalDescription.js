import { useCatalogContext } from "../../../catalog/CatalogContext";

const ModalDescription = () => {
  const { productModal } = useCatalogContext();
  return <p className="modal__description-info">{productModal.description}</p>;
};

export default ModalDescription;
