import NavButton from "../../../../common/Button";

const ModalAddButton = () => {
  return (
    <div className="modal-action">
      <NavButton
        className="modal-action__button "
        value="add to my inventory"
      />
    </div>
  );
};

export default ModalAddButton;
