import { useCatalogContext } from "../../../catalog/CatalogContext";

const ModalTitle = () => {
  const { productModal } = useCatalogContext();
  return <h2 className="modal-title">{productModal.title}</h2>;
};

export default ModalTitle;
