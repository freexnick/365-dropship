import { useCatalogContext } from "../../../catalog/CatalogContext";
const ModalValue = () => {
  const {
    productModal: { price },
  } = useCatalogContext();
  return (
    <div className="modal-value">
      <div className="modal-value__list  modal-value__list--rrp">
        <span className="modal-value__item modal-value__item--number">
          ${Math.round(Math.random() * 100)}
        </span>
        <span className="modal-value__item modal-value__item--text">rrp</span>
      </div>
      <span className="modal-value__separator"></span>
      <div className="modal-value__list modal-value__list--price">
        <span className="modal-value__item modal-value__item--number">
          ${price}
        </span>
        <span className="modal-value__item modal-value__item--text">cost</span>
      </div>
      <span className="modal-value__separator"></span>
      <div className="modal-value__list modal-value__list--profit">
        <span className="modal-value__item modal-value__item--number">
          {Math.round(Math.random() * 100)}% ($
          {Math.round(Math.random() * 100)})
        </span>
        <span className="modal-value__item modal-value__item--text">
          profit
        </span>
      </div>
    </div>
  );
};

export default ModalValue;
