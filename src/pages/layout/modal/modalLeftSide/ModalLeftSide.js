import ModalValue from "./ModalValue";
import ModalDisplayImage from "./ModalDisplayImage";
import ModalGallery from "./ModalGallery";

const ModalLeftSide = () => {
  return (
    <div className="modal-view">
      <ModalValue />
      <ModalDisplayImage />
      <ModalGallery />
    </div>
  );
};

export default ModalLeftSide;
