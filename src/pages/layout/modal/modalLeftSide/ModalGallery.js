import { handleBrokenImg } from "../../../../utils/handleBrokenImg";
import { useCatalogContext } from "../../../catalog/CatalogContext";

const ModalGallery = () => {
  const {
    productModal: { title, image, imageUrl },
  } = useCatalogContext();
  return (
    <div className="modal-slideshow">
      <ul className="modal-gallery">
        <li className="modal-gallery__item">
          <img onError={handleBrokenImg} src={image || imageUrl} alt={title} />
        </li>
      </ul>
    </div>
  );
};

export default ModalGallery;
