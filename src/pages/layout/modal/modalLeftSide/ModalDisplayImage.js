import { handleBrokenImg } from "../../../../utils/handleBrokenImg";
import { useCatalogContext } from "../../../catalog/CatalogContext";

const ModalDisplayImage = () => {
  const {
    productModal: { title, image, imageUrl },
  } = useCatalogContext();

  return (
    <div className="modal__image">
      <img onError={handleBrokenImg} src={image || imageUrl} alt={title} />
    </div>
  );
};

export default ModalDisplayImage;
