import loadGif from "../../../assets/loading.gif";
import { handleBrokenImg } from "../../../utils/handleBrokenImg";

const ProductBody = ({ img, title }) => (
  <div className="catalog__photo">
    <img onError={handleBrokenImg} src={img ? img : loadGif} alt={title} />
  </div>
);

export default ProductBody;
