import Checkbox from "../../../common/Checkbox";
import Button from "../../../common/Button";
import { useInventoryContext } from "../../inventory/InventoryContext";
import { useLocation } from "react-router-dom";

const ProductHeader = ({
  selectedProduct,
  id,
  title,
  handleProductCheckbox,
  product,
}) => {
  const location = useLocation();
  const { handleCartAction } = useInventoryContext();

  return (
    <div className="catalog__item-header" onClick={(e) => e.stopPropagation()}>
      <Checkbox
        title={title}
        handleProductCheckbox={handleProductCheckbox}
        id={id}
        isChecked={selectedProduct.includes(id)}
      />
      <div
        className="product__item-add"
        onClick={
          location.pathname.includes("catalog") ||
          location.pathname.includes("inventory")
            ? () => handleCartAction(product)
            : null
        }
      >
        <Button
          className="catalog__header-button"
          value={
            location.pathname.includes("catalog") ||
            location.pathname.includes("product")
              ? "add to inventory"
              : "remove item"
          }
        />
      </div>
    </div>
  );
};

export default ProductHeader;
