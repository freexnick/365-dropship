const ProductFooter = ({ title, price, qty }) => (
  <>
    <div className="catalog__item-title">
      <span>{title}</span>
    </div>
    <div className="catalog__item-details">
      <div className="catalog__item-supplier">
        <span>By: </span>
        <span>SP-Supplier115</span>
      </div>
      {qty ? (
        <div className="catalog__item-quantity">Quantity:{qty}</div>
      ) : null}
    </div>
    <div className="catalog__values">
      <div className="catalog__value-list catalog__values-list--rrp">
        <span className="catalog__value-item catalog__value-item--number">
          ${Math.round(Math.random() * 100)}
        </span>
        <span className="catalog__value-item catalog__value-item--text">
          RRP
        </span>
      </div>
      <span className="catalog__value-separator catalog__value-separator--price"></span>
      <div className="catalog__value-list catalog__value-list--price">
        <span className="catalog__value-item catalog__value-item--number">
          ${price}
        </span>
        <span className="catalog__value-item catalog__value-item--text">
          cost
        </span>
      </div>
      <span className="catalog__value-separator catalog__value-separator--profit"></span>
      <div className="catalog__value-list catalog__value-list--profit">
        <span className="catalog__value-item catalog__value-item--number">
          {Math.round(Math.random() * 100)}% (${price})
        </span>
        <span className="catalog__value-item catalog__value-item--text">
          profit
        </span>
      </div>
    </div>
  </>
);

export default ProductFooter;
