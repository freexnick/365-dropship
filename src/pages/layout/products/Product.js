import ProductHeader from "./ProductHeader";
import ProductBody from "./ProductBody";
import ProductFooter from "./ProductFooter";
import { useCatalogContext } from "../../catalog/CatalogContext";
import { useLocation } from "react-router-dom";

const Product = ({ id, title, image, price, product, qty }) => {
  const location = useLocation();
  const { selectedProduct, setSelectedProduct, handleProductModal } =
    useCatalogContext();

  const handleProductCheckbox = () => {
    setSelectedProduct([id, ...selectedProduct]);
    if (selectedProduct.includes(id)) {
      setSelectedProduct(selectedProduct.filter((item) => item !== id));
    }
  };

  return (
    <div
      className={`catalog__item ${
        selectedProduct.includes(id) ? "catalog__item--active" : ""
      }`}
      id={id}
      onClick={
        location.pathname.includes("catalog") ||
        location.pathname.includes("inventory")
          ? (e) => handleProductModal(e, product)
          : null
      }
    >
      <ProductHeader
        id={id}
        title={title}
        selectedProduct={selectedProduct}
        handleProductCheckbox={handleProductCheckbox}
        product={product}
      />
      <ProductBody img={image} alt={title} />
      <ProductFooter title={title} price={price} qty={qty} />
    </div>
  );
};

export default Product;
