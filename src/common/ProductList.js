import { useEffect } from "react";
import { getProducts } from "../api/catalogApi";
import Product from "../pages/layout/products/Product";
import { useCatalogContext } from "../pages/catalog/CatalogContext";

const ProductList = () => {
  const { products, displayProducts, setDisplayProducts, setIsLoading } =
    useCatalogContext();

  const loadProducts = async () => {
    if (products) {
      setDisplayProducts(products);
    } else {
      const items = await getProducts();
      localStorage.setItem("products", JSON.stringify(items.data.data));
      setDisplayProducts(items.data.data);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    loadProducts();
  }, []);

  return displayProducts.map((product) => {
    return (
      <Product
        key={product.id}
        {...product}
        product={product}
        image={product.imageUrl}
      />
    );
  });
};

export default ProductList;
