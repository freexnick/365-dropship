import loadGif from "../assets/loading.gif";

const Loading = ({ quantity }) => {
  return [...Array(quantity)].map((el, num) => {
    return (
      <div className="catalog__item catalog__item--load" key={num}>
        <div className="catalog__item-image catalog__item-image--load">
          <img
            className="catalog__item-photo catalog__item-photo--load"
            src={loadGif}
            alt="loading"
          />
        </div>
      </div>
    );
  });
};

export default Loading;
