const NavButton = ({ className, value }) => (
  <button className={className}>{value}</button>
);

export default NavButton;
