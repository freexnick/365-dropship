const Checkbox = ({ id, title, isChecked, handleProductCheckbox }) => {
  return (
    <div className="catalog__checkbox">
      <label htmlFor={id} onClick={handleProductCheckbox}>
        <input
          className="catalog__checkbox-item catalog__checkbox-item--default"
          id={id}
          name={title}
          type="checkbox"
          checked={isChecked}
          onChange={handleProductCheckbox}
        />
        <div className="catalog__checkbox-item catalog__checkbox-item--custom"></div>
      </label>
    </div>
  );
};

export default Checkbox;
