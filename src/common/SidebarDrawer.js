import { Drawer, makeStyles } from "@material-ui/core";
import SideBar from "../pages/catalog/sidebar/Sidebar";
import { useCatalogContext } from "../pages/catalog/CatalogContext";

const useStyles = makeStyles((theme) => ({
  sidebarDrawer: {
    width: "257px",
    position: "unset",
  },
  drawerPaper: {
    left: "unset",
    overflow: "hidden",
    backgroundColor: "#ecedf5",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },
}));

const SidebarDrawer = () => {
  const { matchesSidebarDrawer, sidebarDrawer } = useCatalogContext();
  const classes = useStyles();
  return (
    <Drawer
      variant={matchesSidebarDrawer ? "temporary" : "persistent"}
      anchor="left"
      open={sidebarDrawer}
      className={classes.sidebarDrawer}
      classes={{ paper: classes.drawerPaper }}
    >
      <SideBar />
    </Drawer>
  );
};

export default SidebarDrawer;
