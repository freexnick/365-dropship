import searchIcon from "../assets/search.png";
import cross from "../assets/closeIcon.png";
import { useCatalogContext } from "../pages/catalog/CatalogContext";

const Search = ({ toggleSearch, toggledSearch }) => {
  const { userInput, setUserInput } = useCatalogContext();
  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <>
      <form className="nav__search-form" onSubmit={handleSubmit}>
        <div className="nav__search">
          <input
            className={`nav__search-querry ${
              toggledSearch ? "nav__search-querry--show" : ""
            }`}
            type="text"
            onChange={(e) =>
              setUserInput({ ...userInput, searchTerm: e.target.value })
            }
            value={userInput.searchTerm}
            placeholder="search..."
          />
          <img
            className="nav__search-image nav__search-image--bar"
            src={searchIcon}
            alt="search"
          />
          {toggledSearch && (
            <img
              className="nav__search-image nav__search-image--cross"
              src={cross}
              alt="close"
              onClick={toggleSearch}
            />
          )}
        </div>
        {!toggledSearch && (
          <img
            className=" nav__search-image nav__search-image--toggle"
            src={searchIcon}
            alt="search"
            onClick={toggleSearch}
          />
        )}
      </form>
    </>
  );
};

export default Search;
