import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  drawer: {
    width: "54px",
    [theme.breakpoints.down("md")]: {
      width: "200px",
    },
  },
  drawerPaper: {
    width: "54px",
    left: "unset",
    overflow: "hidden",
    backgroundColor: "#ffffff",
    [theme.breakpoints.down("md")]: {
      width: "200px",
    },
  },
  productCatalog: {
    flexGrow: "1",
    maxWidth: "unset",
  },
}));
