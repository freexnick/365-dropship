import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import "./css/style.comp.css";
import App from "./App";
import "./fonts/fonts.css";
import { NavProvider } from "./pages/layout/nav/NavContext";
import { theme } from "./theme";
import { ThemeProvider } from "@material-ui/core/styles";
import { AuthorizationProvider } from "./pages/user/authorization/AuthorizationContext";

ReactDOM.render(
  <>
    <Router>
      <ThemeProvider theme={theme}>
        <AuthorizationProvider>
          <NavProvider>
            <App />
          </NavProvider>
        </AuthorizationProvider>
      </ThemeProvider>
    </Router>
  </>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
