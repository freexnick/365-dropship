import {
  Route,
  Switch,
  Redirect,
  useHistory,
  useLocation,
} from "react-router-dom";
import NavMenu from "./pages/layout/nav/NavMenu";
import ProductCatalog from "./pages/catalog/ProductCatalog";
import Authorization from "./pages/user/authorization/Authorization";
import { CatalogProvider } from "./pages/catalog/CatalogContext";
import { useNavContext } from "./pages/layout/nav/NavContext";
import { Grid, Drawer } from "@material-ui/core";
import { handleDrawer } from "./utils/handleDrawer";
import { useAuthorizationContext } from "./pages/user/authorization/AuthorizationContext";
import { useEffect } from "react";
import { useStyles } from "./appStyles";
import Inventory from "./pages/inventory/Inventory";
import { InventoryProvider } from "./pages/inventory/InventoryContext";
import ProductPage from "./pages/product/ProductPage";
import { ProductContextProvider } from "./pages/product/ProductContext";

const App = () => {
  const { isLogged, userToken } = useAuthorizationContext();
  const { navDrawer, setNavDrawer, matchesNavDrawer } = useNavContext();
  const location = useLocation();
  const history = useHistory();
  const classes = useStyles();

  useEffect(() => {
    if (!userToken) {
      history.push("/user/login");
      if (location.pathname.includes("register"))
        history.push("/user/register");
    }
  }, [userToken]);

  return (
    <Grid
      container
      onClick={
        matchesNavDrawer ? (e) => handleDrawer(e, setNavDrawer, false) : null
      }
    >
      <Drawer
        variant={matchesNavDrawer ? "temporary" : "persistent"}
        anchor={matchesNavDrawer ? "right" : "left"}
        open={navDrawer}
        className={classes.drawer}
        classes={{ paper: classes.drawerPaper }}
      >
        <Grid item>
          <NavMenu />
        </Grid>
      </Drawer>
      <Switch>
        <Route exact path="/">
          <Redirect to="/catalog" />
        </Route>
        <Route exact path="/user/:action">
          <Authorization />
        </Route>
        <Route path="/inventory/:category?/:id?">
          <InventoryProvider>
            <CatalogProvider>
              <Grid item lg={11} className={classes.productCatalog}>
                {isLogged && <Inventory />}
              </Grid>
            </CatalogProvider>
          </InventoryProvider>
        </Route>
        <Route path="/catalog/:category?/:id?">
          <InventoryProvider>
            <CatalogProvider>
              <Grid item lg={11} className={classes.productCatalog}>
                <ProductCatalog />
              </Grid>
            </CatalogProvider>
          </InventoryProvider>
        </Route>
        <Route path="/product/:id?">
          <InventoryProvider>
            <CatalogProvider>
              <ProductContextProvider>
                <Grid item lg={11} className={classes.productCatalog}>
                  <ProductPage />
                </Grid>
              </ProductContextProvider>
            </CatalogProvider>
          </InventoryProvider>
        </Route>
      </Switch>
    </Grid>
  );
};

export default App;
